// Almacenamos los objetos

var cosas = [   "J Dilla - Dilla's Mix The Instrumentals, descripcion del objeto, img/jdilla.jpg, 50",
                "The Alan Parsons Project - PYRAMID, descripcion del objeto, img/theallanparsons.jpg, 37",
                "Pink Floyd - The Dark Side Of The Moon, descripcion del objeto, img/pinkfloid.jpg, 45"
            ];


// Objeto con la cesta de la compra

var cestaCompra = {};


// Declaro la clase Disco para instanciar los articulos con método comprar que gestionara la compra, actualiza el stock y devuelve la cantidad si la operación se ha podido realizar y propiedades de los artículos

var Disco = function(nombre, descripcion, link, precio) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.link = link;
    this.precio = precio;
    this.stock = 10;

    this.comprar = function(cantidad) {
        if (this.stock > 0) {
            this.stock --;
            return cantidad;
        } 
        return 0;
    }
};


// Separamos por comas los strings de cosas

var arrayProductos = cosas.map(function(elem){
    return elem.split(',');
});


// Construimos un array con los objetos instancias

var objetos = []; 

arrayProductos.forEach(function(elem){
  var disco = new Disco(elem[0].trim(),elem[1].trim(),elem[2].trim(),elem[3]);
  objetos.push(disco);
});


// Seleccionamos la plantilla del HTML

var plantilla = document.querySelector('#item');
var tarjeta = plantilla.content.querySelector('#tarjeta');


// Pintamos en el HTML tantas veces la plantilla como objetos tengamos relleno con los datos de cada objeto que está en el array y le asigno el evento al botón para realizar la compra

for (i = 0; i < objetos.length; i++) {

    var tar = tarjeta.cloneNode(true);

    tar.id = 'elemDiv' + i;
    tar.querySelector('img').src = objetos[i].link;
    tar.querySelector('#nombre').textContent = objetos[i].nombre;
    tar.querySelector('#precio').textContent = objetos[i].precio + '€'; 
    
    var btnProducto = tar.querySelector('button');
    btnProducto.id = i;
    btnProducto.addEventListener('click', compra);

    var clonar = plantilla.content.cloneNode(true);
    document.querySelector('#colItems').appendChild(tar);

};


// Recogemos el elemento clicado y llamamos a su método de compra, pinto un mensaje temporal según el resultado de la operación y añado a la cesta el artículo comprado

function compra(event) {

    // Identifico el botón que recibe el evento, el objeto al que llama ese botón y la tarjeta que lo contiene en el html
    var idBoton = event.target.id;
    var articulo = objetos[idBoton];
    var elemDiv = document.querySelector('#elemDiv'+idBoton);

    // Le doy la cantidad a sumar cada vez que se compra un artículo
    var cantidad = 1;

    // Se lo resto a la catidad de stock
    var resta = articulo.comprar(cantidad);

    // Creo una variable para introducir el mensaje temporal
    let textoAdd = '';

    if ( resta > 0 ) {
            textoAdd = `Se ha añadido ${resta} artículo a su cesta de la compra.`;
            anadirACesta(articulo.nombre, cantidad); 
    } else {
            textoAdd = `No disponemos de suficiente stock`;     
    }

    // Inserto el mensaje temporal de el elemento con la clase stock del html
    elemDiv.querySelector('.stock').innerHTML = textoAdd;
    
    // Muestro el mensaje durante 3 segundos
    setTimeout(function(){ 
        elemDiv.querySelector('.stock').innerHTML = ''; 
    }, 3000);
};


// Añadimos propiedades a la cesta con el nombre como clave y la cantidad como valor y pintamos de nuevo temporalmente la cesta de la compra

function anadirACesta(clave, cantidad) {

    // Esta línea está copiada del profesor para evitar bloquearme en este momento (Entender)
    cestaCompra[clave] = cestaCompra[clave] ? cestaCompra[clave] + cantidad : cantidad;
    // Actualizo la cesta
    actualizarCesta();
    // Mostramos la cesta durante 10 segundos
    setTimeout(function(){ 
        document.querySelector('#cesta').classList.add('invisible'); 
    }, 10000);

}


// Busco en el array de objetos el objeto por su nombre y me devuelve el objeto

function saberNombre(texto) {

    for(var i = 0; i < objetos.length; i++) {
        if (objetos[i].nombre == texto) return objetos[i];
    } 
    return null;

}

// console.log(saberNombre('TILLSYN - Reloj de arena decorativo'));


// Uso el template de itemsCesta para pintar la cesta, muestro la cesta con los elementos almacenados. Calculo y mouestro el total de la compra

function actualizarCesta() {

    // Selecciono el template
    var template = document.querySelector('#itemsCesta');
    var divItemsCesta = template.content.querySelector('div');

    // Pinto la Cesta
    var cestaDiv = document.querySelector('#cesta');
    // Le quito la clase invisible
    cestaDiv.classList.remove('invisible');
    // Reinicio los valores de la cesta
    cestaDiv.innerHTML = '';
    // Total de la cesta al inicializar
    var costeCesta = 0;

    

    // Recorremos la cesta
    // Para cada elemento de la cesta
    for (element in cestaCompra) {
        var disco = saberNombre(element);
        var clonDiv = divItemsCesta.cloneNode(true);
        var cantidades = cestaCompra[element];

        clonDiv.querySelector('.nombre').innerHTML = disco.nombre;
        clonDiv.querySelector('.cantidad').innerHTML = cantidades;
        clonDiv.querySelector('.precio').innerHTML = disco.precio + '€';
        cestaDiv.appendChild(clonDiv);
        
        // Sumamos a la cesta cada vez que se compra un artículo
        costeCesta += cantidades * disco.precio; 
    };
 
    // Creo una variable total para mostrar el total acumulado de la compra y lo agrego al html con un id 'total' para modificar su estilo en css
    var total = document.createElement('div');
    total.id = 'total';
    total.innerHTML = `<strong>PRECIO TOTAL: ${costeCesta}€</strong>`;
    cestaDiv.appendChild(total);

}
